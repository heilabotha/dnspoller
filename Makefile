.PHONY: build
build:
	CGO_ENABLED=0 go build -v -ldflags "-s -w" ./cmd/dns-poller

.PHONY: poll
poll:
	curl -X POST http://127.0.0.1:8080/startPolling -d '[ {"txtName": "oly-hackday.sandbox.entersekt.com", "txtRecord": "test4" }, {"txtName": "oly-hackday2.sandbox.entersekt.com", "txtRecord": "test4" }]'

.PHONY: result
result:
	curl -X POST http://127.0.0.1:8080/getPollResult -d '[ { "txtName": "oly-hackday.sandbox.entersekt.com"}, {"txtName": "oly-hackday.sandbox.entersekt.com"} ]'