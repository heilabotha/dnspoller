package main

import (
	log "github.com/sirupsen/logrus"
	dns "gitlab.com/heilabotha/dnspoller.git/pkg/dns"
)

func main() {
	log.Info("Starting CSR Receiver")

	srv := dns.NewService()
	srv.Init()
	srv.Run()
}
