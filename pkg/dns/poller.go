package dns

import (
	"fmt"
	"net"
	"sync"
	"time"
)

const (
	statusInProgress = "IN_PROGRESS"
	statusError      = "ERROR"
	statusValidated  = "VALIDATED"
	statusExpired    = "EXPIRED"
)

type dbEntry struct {
	txtName   string
	txtRecord string
	created   time.Time
	status    string
}

type db interface {
	get(key string) (dbEntry, error)
	put(key string, e dbEntry)
}

type dbMock struct {
	sync.RWMutex
	m map[string]dbEntry
}

func (d *dbMock) put(key string, e dbEntry) {
	d.Lock()
	defer d.Unlock()
	d.m[key] = e
}

func (d *dbMock) get(key string) (dbEntry, error) {
	d.Lock()
	defer d.Unlock()
	e, found := d.m[key]
	if !found {
		return dbEntry{}, fmt.Errorf("entry does not exist")
	}
	return e, nil
}

type poller struct {
	store db
	done  chan struct{}
}

func (p *poller) startPolling(txtName, txtRecord string) {
	e := dbEntry{
		txtName:   txtName,
		txtRecord: txtRecord,
		created:   time.Now(),
		status:    statusInProgress,
	}

	p.store.put(txtName, e)

	go func(store db) {
		t := time.NewTicker(5 * time.Second)
		defer t.Stop()
		for {
			select {
			case <-p.done:
				return
			case <-t.C:
				log.Printf("Looking for record %v = %v", txtName, txtRecord)
				e, err := store.get(txtName)
				if err != nil {
					log.Errorf("Could not find entry: %v", txtName)
					return
				}

				if time.Since(e.created) > time.Hour {
					log.Warnf("Entry expired: %v", txtName)
					e.status = statusExpired
					store.put(e.txtName, e)
					return
				}

				validated, err := p.poll(txtName, txtRecord)
				if err != nil {
					log.Warnf("Error checking txt record: %v", txtName)

					e.status = statusError
					store.put(e.txtName, e)
					continue
				}

				if validated {
					log.Infof("Validated record: %v", txtName)
					e.status = statusValidated
					store.put(e.txtName, e)
					return
				}

			}
		}
	}(p.store)
}

func (p *poller) poll(txtName, txtRecord string) (bool, error) {
	txtRecords, err := net.LookupTXT(txtName)
	if err != nil {
		return false, err
	}

	for _, txt := range txtRecords {
		if txt == txtRecord {
			return true, nil
		}
	}

	return false, nil
}

func (p *poller) getResult(txtName string) string {
	e, err := p.store.get(txtName)
	if err != nil {
		return statusError
	}

	return e.status
}

func (p *poller) shutdown() {
	log.Info("Shutting down")
	close(p.done)
	time.Sleep(5 * time.Second)
}
