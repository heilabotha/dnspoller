package dns

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"github.com/sirupsen/logrus"

	"github.com/gorilla/mux"
)

var log = logrus.WithField("ctx", "dns")

type Service struct {
	router *mux.Router
	p      *poller
}

func NewService() *Service {
	return &Service{
		router: mux.NewRouter(),
		p:      &poller{store: &dbMock{m: make(map[string]dbEntry)}},
	}
}

type StartPollingReq []struct {
	TxtName   string `json:"txtName"`
	TxtRecord string `json:"txtRecord"`
}

type GetPollResultReq []struct {
	TxtName string `json:"txtName"`
}

type GetPollResultRes []struct {
	TxtName string `json:"txtName"`
	Status  string `json:"status"`
}

func (s *Service) Init() {
	s.router.HandleFunc("/startPolling", s.startPolling).Methods("POST")
	s.router.HandleFunc("/getPollResult", s.getPollResult).Methods("POST")
	s.router.HandleFunc("/", s.healthcheck).Methods("GET")
}

func (s *Service) Run() {
	const addr = "0.0.0.0:8080"
	log.WithField("addr", addr).Info("Starting listener")
	if err := http.ListenAndServe(addr, s.router); err != nil {
		log.WithError(err).Error("Listener exited")
	}
	log.Info("Listener stopped normally")
	s.p.shutdown()
}

func (s *Service) healthcheck(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

func (s *Service) startPolling(w http.ResponseWriter, r *http.Request) {
	log.Info("Received startPolling")
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Error("startPolling error")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var req StartPollingReq
	err = json.Unmarshal(body, &req)
	if err != nil {
		log.WithError(err).Error("startPolling error")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	for _, e := range req {
		s.p.startPolling(e.TxtName, e.TxtRecord)
	}
	w.WriteHeader(http.StatusOK)
}

func (s *Service) getPollResult(w http.ResponseWriter, r *http.Request) {
	log.Info("Received getPollResult")
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.WithError(err).Error("getPollResult error")
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	var req GetPollResultReq
	err = json.Unmarshal(body, &req)
	if err != nil {
		log.WithError(err).Error("getPollResult error")
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	var res GetPollResultRes
	for _, e := range req {
		status := s.p.getResult(e.TxtName)
		res = append(res, struct {
			TxtName string "json:\"txtName\""
			Status  string "json:\"status\""
		}{TxtName: e.TxtName, Status: status})
	}

	resBody, err := json.Marshal(res)
	if err != nil {
		log.WithError(err).Error("getPollResult error")
		w.WriteHeader(http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	w.Write(resBody)
}
