# Build stage
FROM golang:1.16.0-buster AS build-env

COPY . /src
WORKDIR /src

RUN make build

# Image stage
FROM debian:stable-slim
LABEL maintainer="Olympus <olympus@entersekt.com>"

# Define exposed ports for validation with the swarm-exporter
EXPOSE 8080

RUN apt-get update && apt-get install -y \
    curl \
    && rm -rf /var/lib/apt/lists/*

COPY --from=build-env /src/dns-poller /usr/bin/dns-poller
RUN chmod +x /usr/bin/dns-poller

HEALTHCHECK --interval=30s --retries=3 --timeout=10s CMD curl -f 'http://localhost:8080/' > /dev/null 2>&1 || exit 1

RUN addgroup --system entersekt && \
    adduser --system --no-create-home --group entersekt
USER entersekt

ENTRYPOINT ["/usr/bin/dns-poller"]
