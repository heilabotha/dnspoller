module gitlab.com/heilabotha/dnspoller.git

go 1.14

require (
	github.com/gorilla/mux v1.8.0
	github.com/sirupsen/logrus v1.8.1
)

replace gitlab.com/heilabotha/dnspoller.git/pkg => ./pkg/
